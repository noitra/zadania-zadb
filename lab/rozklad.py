from math import *
import memcache, random, time, timeit

mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])

found = 0
total = 0
 
def rozklad_na_czynniki(input):
    if input<=0:
        return 0
    if input > 1:
        i=2
        pierwiastek=floor(sqrt(input))
        tab=[]
        while i<=pierwiastek:
            if input%i==0:
                tab.append(i)
                input/=i
                pierwiastek=floor(sqrt(input))
            else:
                i+=1
        if input>1: tab.append(input)
    return tab

def rob_rozklad(n):
        global total, found
        total += 1
        value = mc.get(str(n))
        if value is None:
                time.sleep(0.003)
                value = rozklad_na_czynniki(n)
                mc.set(str(n), value)

        else:
                found += 1
        print('liczba: ' + str(n) + ' ma rozklad: ' + str(value))
        return value

def make_request():
        rob_rozklad(random.randint(0, 50000000))

print('Ten successive runs:',)
for i in range(1, 11):
        print('%.2fs, ratio=%.4f' % (timeit.timeit(make_request, number=200), float(found)/total))
        print