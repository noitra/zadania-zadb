import pika, random, timeit, uuid, requests, json
#mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])

connection = pika.BlockingConnection(pika.ConnectionParameters(
                host='194.29.175.241'))

channel = connection.channel()
channel.queue_declare(queue='random_sorting_queue')

result = channel.queue_declare(exclusive=True)
callback_queue = result.method.queue

response = {}

def on_response(ch, method, props, body):
        corr_id = props.correlation_id
        response[corr_id] = body

def send_numbers(n):
        corr_id = str(uuid.uuid4())
        response[corr_id] = None
        channel.basic_publish(exchange='',
                           routing_key='random_sorting_queue',
                           properties=pika.BasicProperties(
                                 reply_to = callback_queue,
                                 correlation_id = corr_id,
                           ),
                           body=str(n))


def make_request():
        listOfFiguresFromRandomOrgSite = requests.get("http://www.random.org/integers/?num=10&min=1&max=9&col=1&base=10&format=plain&rnd=new")
        send_numbers(listOfFiguresFromRandomOrgSite.text.replace("\n", ' '))
        print("Odbieram dane z serwera Random.org...")
        for number in listOfFiguresFromRandomOrgSite:
            print(number)

def requests_suite():
        for i in range(5):
                make_request()
        while None in response.values():
                connection.process_data_events()

channel.basic_consume(on_response, queue=callback_queue)

print 'Ten successive runs:',
for i in range(1, 11):
        print '%.2fs' % timeit.timeit(requests_suite, number=1)
        print

connection.close()