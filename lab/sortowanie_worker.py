#!/usr/bin/env python
import pika, time

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='194.29.175.241'))

channel = connection.channel()

channel.queue_declare(queue='random_sorting_queue')


def sort_numbers(listOfFiguresToSort):
        time.sleep(0.005)
        listOfFiguresToSort.sort()
        return listOfFiguresToSort

def on_request(ch, method, props, body):
        listOfFigures = []
        splitBody = body.split(' ')
        for number in splitBody:
            if not number == '':
                listOfFigures.append(int(number))
        response = sort_numbers(listOfFigures)
        print('Odebrana i posortowana lista liczb:')
        print(listOfFigures)
        ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=str(response))
        ch.basic_ack(delivery_tag = method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='random_sorting_queue')

channel.start_consuming()