# -*- coding: utf-8 -*-

import os
import googlr
import unittest
import tempfile
from flask import session


class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        self.app = googlr.app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        self.db_fd, self.app.config['DATABASE'] = tempfile.mkstemp()
        # inicjalizacja bazy
        googlr.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.app.config['DATABASE'])

    def test_database_setup(self):
        con = googlr.connect_db()
        cur = con.execute('PRAGMA table_info(entries);')
        rows = cur.fetchall()
        self.assertEquals(len(rows), 4)

    def test_write_entry(self):
        expected = (u"użytkownik", u"zawartość")
        with self.app.test_request_context('/'):
            self.app.preprocess_request()
            googlr.write_entry(*expected)
            con = googlr .connect_db()
            cur = con.execute("select * from entries;")
            rows = cur.fetchall()
        self.assertEquals(len(rows), 1)
        for val in expected:
            self.assertTrue(val in rows[0])

    def test_get_all_entries_empty(self):
        with self.app.test_request_context('/'):
            self.app.preprocess_request()
            entries = googlr.get_all_entries()
            self.assertEquals(len(entries), 0)

    def test_get_all_entries(self):
        expected = (u"użytkownik", u"zawartość")
        with self.app.test_request_context('/'):
            self.app.preprocess_request()
            googlr.write_entry(*expected)
            entries = googlr.get_all_entries()
            self.assertEquals(len(entries), 1)
            for entry in entries:
                self.assertEquals(expected[0], entry['username'])
                self.assertEquals(expected[1], entry['query'])

    def test_empty_listing(self):
        response = self.client.get('/')
        assert u'Brak danych' in unicode(response.data, 'utf-8')

    def test_listing(self):
        expected = (u"użytkownik", u"zawartość")
        with self.app.test_request_context('/'):
            self.app.preprocess_request()
            googlr.write_entry(*expected)
        response = self.client.get('/')
        for value in expected:
            assert value in unicode(response.data, 'utf-8')

    # def test_login_passes(self):
    #     """
    #     Testuje poprawne logowanie.
    #     """
    #     # Symulacja ĹźÄdania z URL = /
    #     with self.app.test_request_context('/'):
    #         # WywoĹanie preprocesingu dekoratorĂłw
    #         self.app.preprocess_request()
    #         # Logowanie za pomocÄ dedykowanej funkcji z poprawnymi danymi
    #         microblog.do_login(self.app.config['USERNAME'],
    #                            self.app.config['PASSWORD'])
    #         self.assertTrue(session.get('logged_in', False))
    #
    # def test_login_fails(self):
    #     """
    #     Testuje bĹÄdne logowanie.
    #     """
    #     # Symulacja ĹźÄdania z URL = /
    #     with self.app.test_request_context('/'):
    #         # WywoĹanie preprocesingu dekoratorĂłw
    #         self.app.preprocess_request()
    #         # Logowanie za pomocÄ dedykowanej funkcji z bĹÄdnymi danymi
    #         self.assertRaises(ValueError, microblog.do_login,
    #                           self.app.config['USERNAME'],
    #                           'BledneHaslo')
    #
    # def login(self, username, password):
    #     """
    #     Funkcja pomocnicza do logowania.
    #     """
    #     return self.client.post('/login', data=dict(
    #         username=username,
    #         password=password
    #     ), follow_redirects=True)
    #
    # def logout(self):
    #     """
    #     Funkcja pomocnicza do wylogownia.
    #     """
    #     return self.client.get('/logout',
    #                            follow_redirects=True)
    #
    # def test_login_logout(self):
    #     """
    #     Testuje caĹy proces logowania z poprawnymi i bĹÄdnymi danymi.
    #     """
    #     response = self.login(self.app.config['USERNAME'],
    #                           self.app.config['PASSWORD'])
    #     assert u'ZostaĹeĹ zalogowany' in unicode(response.data, 'utf-8')
    #     response = self.logout()
    #     assert u'ZostaĹeĹ wylogowany' in unicode(response.data, 'utf-8')
    #     response = self.login('blad',
    #                           self.app.config['PASSWORD'])
    #     assert u'BĹÄdne dane' in unicode(response.data, 'utf-8')
    #     response = self.login(self.app.config['USERNAME'],
    #                           'blad')
    #     assert u'BĹÄdne dane' in unicode(response.data, 'utf-8')
    #
    # def test_add_entries(self):
    #     """
    #     Testuje dodawanie wpisu za pomocÄ widoku
    #     po uporzednim zalogowaniu siÄ.
    #     """
    #     self.login(self.app.config['USERNAME'],
    #                self.app.config['PASSWORD'])
    #     response = self.client.post('/add', data=dict(
    #         title=u'CzeĹÄ',
    #         text=u'To jest treĹÄ wpisu'
    #     ), follow_redirects=True)
    #     assert u'Jak dotÄd brak wpisĂłw' not in unicode(response.data, 'utf-8')
    #     assert u'CzeĹÄ' in unicode(response.data, 'utf-8')
    #     assert u'To jest treĹÄ wpisu' in unicode(response.data, 'utf-8')


if __name__ == '__main__':
    unittest.main()