# -*- coding: utf-8 -*-
import Image
import os
import zipfile

from flask import Flask, render_template, send_from_directory
from flask import session
from flask import request, redirect
from flask import flash, url_for
from celery import Celery


DATABASE = 'googlr.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERS = {'admin':'TajneHaslo'}

#pathToImages = '/home/p8/public_html/static/upload'
pathToImages = '/home/p8/dom/static/upload'


app = Flask(__name__)
celery = Celery()
app.config.from_pyfile('config.py')
# app.config.from_object(__name__)

extensions = ['', 'jpeg', 'jpg', 'gif', 'bmp', 'png']


@app.route('/images')
@app.route('/images/<path:catalog_name>/')
def images(catalog_name = ""):
    listOfFiles = os.listdir(pathToImages)
    if len(catalog_name) > 0:
        try:
            listOfFiles = os.listdir(pathToImages+"/"+catalog_name)
        except:
            return render_template('error.html',message="Nie ma takieogo katalogu")
    images=[]
    for file in listOfFiles:
        splittedFileName = file.split(".")
        splittedFileName.append('')
        if splittedFileName[1] in extensions:
            subFolders = pathToImages.split('/')
            currentPath = ""
            for subfolder in subFolders:
                if (subFolders.index(subfolder) >= subFolders.index("upload")):
                    currentPath+=subfolder+"/"
            ###
            if len(catalog_name) > 0:
                catalog_name+="/"
            images.append(currentPath+catalog_name+file)
    return render_template('images.html',images=images)

def retImages(catalog_name = ""):
    files = os.listdir(pathToImages)
    if len(catalog_name) > 0:
        try:
            files = os.listdir(pathToImages+"/"+catalog_name)
        except:
            return render_template('error.html',message="Nie ma takieogo katalogu")
    images=[]
    for file in files:
        splited = file.split(".")
        splited.append('')
        if splited[1] in extensions:
            ###wyszukiwanie podkatalogow
            subfoldery = pathToImages.split('/')
            sciezka = ""
            for subfolder in subfoldery:
                if (subfoldery.index(subfolder) >= subfoldery.index("upload")):
                    sciezka+=subfolder+"/"
                    ###
            if (catalog_name[-1:] != '/' and len(catalog_name)>0):
                catalog_name+='/'
            images.append(sciezka+catalog_name+file)
    return images


@app.route('/image/<path:sciezka>')
def index(sciezka):
    pathForRenderTemplate = sciezka.replace('upload/', '')
    try:
        with open(pathToImages.replace('upload','')+sciezka): pass
    except:
        return render_template('error.html',message="Nie ma takiego pliku")
    return render_template('image_view.html',image=pathForRenderTemplate)

@app.route('/upload')
def upload():
    return render_template('upload_image.html')

def asyncUploaded():
    uploaded.dealy()

@celery.task
@app.route('/uploaded',methods=['GET', 'POST'])
def uploaded():
    success = False
    number = 0
    if request.method == 'POST' and 'image[]' in request.files:
        try:
            files = request.files.getlist('image[]')
            for file in files:
                if file.filename.split('.')[-1] in extensions:
                    file.save(pathToImages+'/'+file.filename)
                    success=True
                    number+=1
                elif file.filename.split('.')[-1]=='zip':
                    zfile=zipfile.ZipFile(file)
                    for name in zfile.namelist():
                        filename = os.path.split(name)[1]
                        if filename.split('.')[-1]in extensions:
                            fd = open(pathToImages+'/'+filename,"w")
                            fd.write(zfile.read(name))
                            fd.close()
                            scale_image(filename, 400)
                            scale_image(filename, 200)
                            scale_image(filename, 100)
                            success=True
                            number+=1
        except:
            return render_template('upload_complete.html',success=success,number=number)
    return render_template('upload_complete.html',success=success,number=number)

@app.route('/zip/')
def zip_images():
    try:
        os.remove(pathToImages+'/Images.zip')
    except:
        pass
    zipArchive = zipfile.ZipFile(pathToImages+'/Images.zip','w')
    files = os.listdir(pathToImages)
    for filename in files:
        if filename.split('.')[-1] in extensions:
            zipArchive.write(pathToImages+'/'+filename,arcname=filename)
    zipArchive.close()
    return send_from_directory(pathToImages,'Images.zip',as_attachment=True)



def zip_image(image_name):
    image_name = image_name.replace("upload/","")
    zipname_parts = image_name.split('.')[:-1]
    zipname=''
    for part in zipname_parts:
        zipname+=part+'.'
    zipname+='zip'
    zip = zipfile.ZipFile(pathToImages+'/'+zipname,'w')
    zip.write(pathToImages+'/'+image_name,arcname=image_name)
    zip.close()
    return send_from_directory(pathToImages,zipname,as_attachment=True)
    zip.delete()


@app.route('/delete/<image_name>/')
def delete_image(image_name):
    try:
        os.remove(pathToImages+'/'+image_name)
    except:
        pass
    return render_template('image_deleted.html')


def scale_image(filename, width1):
    image = Image.open(pathToImages+'/'+filename)

    width, height =image.size
    newim = image.resize((width1,width1*height/width), Image.ANTIALIAS)
    min = filename.split('.')[0]+"_min."+filename.split('.')[1]
    newim.save(pathToImages+'/'+min)
    os.rename(pathToImages+'/'+min, pathToImages+'/'+filename.split(".")[0]+str(width1)+".min")


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if request.form['username'] == 'admin' and request.form['password'] == 'admin':
            #session['username'] = 'admin'
            #session['logIn'] = True
            return redirect(url_for('images'))
    return render_template('login.html')

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('images'))


@app.route('/zip/<path:image_name>/')
def downloadImageAsZipFile(image_name):
    image_name = image_name.replace("upload/","")
    partsOfZipName = image_name.split('.')[:-1]
    zipName=''
    for part in partsOfZipName:
        zipName+=part+'.'
    zipName+='zip'
    zip = zipfile.ZipFile(pathToImages+'/'+zipName,'w')
    zip.write(pathToImages+'/'+image_name,arcname=image_name)
    zip.close()
    return send_from_directory(pathToImages,zipName,as_attachment=True)
    zip.delete()

def update_username():
    if not session.get('username'):
        session['username'] = request.remote_addr

if __name__ == '__main__':
    session = session()
    app.run(host='194.29.175.240',port=31008)